package com.amandalacanna.redditapp.data

import android.graphics.Bitmap


data class ThumbnailData (val bitmap: Bitmap,
            val name: String,
            val description: String)