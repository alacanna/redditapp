package com.amandalacanna.redditapp.data

import java.io.Serializable

data class RedditData (val data: Data): Serializable