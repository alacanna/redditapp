package com.amandalacanna.redditapp.data

import java.io.Serializable

data class Children(val data: ChildrenData): Serializable
