package com.amandalacanna.redditapp.data

import java.io.Serializable

data class Data(var children: MutableList<Children>?,
                var after: String?,
                val before: String?): Serializable