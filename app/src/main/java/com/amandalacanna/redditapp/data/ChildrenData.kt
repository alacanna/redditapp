package com.amandalacanna.redditapp.data

import java.io.Serializable

data class ChildrenData(val author: String,
                        val title: String,
                        val created_utc: Long,
                        val num_comments: Int,
                        val thumbnail: String?,
                        val url: String?): Serializable