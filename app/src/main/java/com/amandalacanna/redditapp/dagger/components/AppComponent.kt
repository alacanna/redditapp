package com.amandalacanna.redditapp.dagger.components

import android.app.Application
import com.amandalacanna.redditapp.application.RedditApplication
import com.amandalacanna.redditapp.dagger.modules.APIModule
import com.amandalacanna.redditapp.dagger.modules.ActivityModule
import com.amandalacanna.redditapp.dagger.modules.AppModule
import com.android.grabovoi.feature.home.adapter.RedditAdapter
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AndroidInjectionModule::class, AppModule::class, ActivityModule::class, APIModule::class))
interface AppComponent {
    fun inject(application: RedditApplication)
    fun inject(adapter: RedditAdapter)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun appModule(appModule: AppModule): Builder
        fun build(): AppComponent
    }
}