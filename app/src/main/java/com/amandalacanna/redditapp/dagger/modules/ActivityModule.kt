package com.amandalacanna.redditapp.dagger.modules

import com.amandalacanna.redditapp.features.toplist.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
internal abstract class ActivityModule {

    @ContributesAndroidInjector()
    internal abstract fun contribuiteMainActivity(): MainActivity
}
