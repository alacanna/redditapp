package com.amandalacanna.redditapp.dagger.modules

import com.amandalacanna.redditapp.api.RestAPI
import com.amandalacanna.redditapp.api.manager.RedditManager
import com.amandalacanna.redditapp.api.request.TopRedditRequest
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class APIModule {

    @Provides
    @Singleton
    internal fun providerApplicationRedditManager(topRedditRequest: TopRedditRequest): RedditManager {
        return RedditManager(topRedditRequest)
    }

    @Provides
    @Singleton
    internal fun providerApplicationTopRedditRequest(): TopRedditRequest {
        return RestAPI().topReddit
    }

}