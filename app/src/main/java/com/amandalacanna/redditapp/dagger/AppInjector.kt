package com.amandalacanna.redditapp.dagger

import android.app.Activity
import android.app.Application
import android.os.Bundle
import com.amandalacanna.redditapp.application.RedditApplication
import com.amandalacanna.redditapp.dagger.components.DaggerAppComponent
import com.amandalacanna.redditapp.dagger.modules.AppModule
import dagger.android.AndroidInjection

object AppInjector {

    lateinit var dagger: DaggerAppComponent

    fun init(application: RedditApplication) {

        dagger = DaggerAppComponent.builder()
                .application(application)
                .appModule(AppModule(application))
                .build() as DaggerAppComponent

        dagger.inject(application)

        application
                .registerActivityLifecycleCallbacks(object : Application.ActivityLifecycleCallbacks {
                    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
                        handleActivity(activity)
                    }

                    override fun onActivityStarted(activity: Activity) {}

                    override fun onActivityResumed(activity: Activity) {}

                    override fun onActivityPaused(activity: Activity) {}

                    override fun onActivityStopped(activity: Activity) {}

                    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {}

                    override fun onActivityDestroyed(activity: Activity) {}
                })
    }

    private fun handleActivity(activity: Activity) {
        AndroidInjection.inject(activity)
    }
}