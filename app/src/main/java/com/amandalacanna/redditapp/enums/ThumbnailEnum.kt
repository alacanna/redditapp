package com.amandalacanna.redditapp.enums

enum class ThumbnailEnum (val desc: String){
    DEFAULT ("default"), SELF("self")
}