package com.amandalacanna.redditapp.extension

import org.joda.time.DateTime
import org.joda.time.Duration
import org.joda.time.DateTimeZone

fun Long.getDuration (): String{
    val dateCreated = DateTime(this * 1000, DateTimeZone.UTC)
    val dateNow = DateTime.now().withZone(DateTimeZone.UTC)
    val duration = Duration( dateCreated , dateNow)
    return String.format("%d hours ago", duration.toStandardHours().hours)
}