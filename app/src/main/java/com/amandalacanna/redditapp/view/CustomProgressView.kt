package com.amandalacanna.redditapp.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.RelativeLayout
import com.amandalacanna.redditapp.R
import kotlinx.android.synthetic.main.custom_progress.view.*


class CustomProgressView : RelativeLayout {
    private var mInflater: LayoutInflater

    constructor(context: Context) : super(context) {
        mInflater = LayoutInflater.from(context)
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        mInflater = LayoutInflater.from(context)
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        mInflater = LayoutInflater.from(context)
        init()
    }

    private fun init() {
        mInflater.inflate(R.layout.custom_progress, this, true)
        progress.setOnClickListener { }
    }
}
