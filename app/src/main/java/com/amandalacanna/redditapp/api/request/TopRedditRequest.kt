package com.amandalacanna.redditapp.api.request

import com.amandalacanna.redditapp.data.RedditData
import io.reactivex.Observable
import retrofit2.http.*

interface TopRedditRequest {
    @Headers("Content-Type: application/json")
    @GET("top/.json")
    fun getTopReddit(@Query("count") count: Int?,
                     @Query("limit") limit: Int,
                     @Query("after") after: String?): Observable<RedditData>
}
