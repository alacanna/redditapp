package com.amandalacanna.redditapp.api

import com.amandalacanna.redditapp.BuildConfig
import com.amandalacanna.redditapp.api.request.TopRedditRequest
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


class RestAPI(httpUrl: HttpUrl) {
    constructor() : this(HttpUrl.parse("https://www.reddit.com/")!!)

    private val loggingInterceptor: HttpLoggingInterceptor by lazy {
        val log = HttpLoggingInterceptor()
        log.level = if (BuildConfig.DEBUG)
            HttpLoggingInterceptor.Level.BODY
        else
            HttpLoggingInterceptor.Level.NONE

        log
    }

    private val httpClient = OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .writeTimeout(90, TimeUnit.SECONDS)
            .readTimeout(90, TimeUnit.SECONDS)
            .connectTimeout(90, TimeUnit.SECONDS)!!


    private val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl(httpUrl)
            .client(httpClient.build())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build())
            .build()

    val topReddit: TopRedditRequest by lazy {
        retrofit.create(TopRedditRequest::class.java)
    }
}

