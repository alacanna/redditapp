package com.amandalacanna.redditapp.api.manager

import com.amandalacanna.redditapp.api.request.TopRedditRequest
import com.amandalacanna.redditapp.data.RedditData
import io.reactivex.Observable


class RedditManager (private val redditRequest: TopRedditRequest){

    fun getTopReddit(after: String?, count: Int?): Observable<RedditData> {
        return redditRequest.getTopReddit(count, 10, after)
    }

}