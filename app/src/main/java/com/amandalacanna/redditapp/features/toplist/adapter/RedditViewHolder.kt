package com.android.grabovoi.feature.home.adapter

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.support.v7.widget.RecyclerView
import android.view.View
import com.amandalacanna.redditapp.R
import com.amandalacanna.redditapp.data.Children
import com.amandalacanna.redditapp.data.ThumbnailData
import com.amandalacanna.redditapp.databinding.RedditItemAdapterBinding
import com.amandalacanna.redditapp.enums.ThumbnailEnum
import com.amandalacanna.redditapp.features.toplist.viewmodel.TopListViewModel
import com.amandalacanna.redditapp.extension.getDuration
import com.jakewharton.rxbinding.view.RxView
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target

class RedditViewHolder (private val binding: RedditItemAdapterBinding, val viewModel: TopListViewModel) : RecyclerView.ViewHolder(binding.root) {
    fun setReddit(reddit: Children) {
        binding.reddit = reddit
    }
    fun setDate(reddit: Children) {
        binding.txtDate.text = reddit.data.created_utc.getDuration()
    }

    fun setThumbnail (reddit: Children){

        Picasso.with(binding.root.context)
                .load(reddit.data.thumbnail)
                .into(object : Target{
                    override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                        binding.imgThumb.visibility = View.GONE
                        binding.loadingItem.visibility = View.VISIBLE
                    }

                    override fun onBitmapFailed(errorDrawable: Drawable?) {
                        binding.imgThumb.visibility = View.VISIBLE
                        binding.imgThumb.setImageResource(R.mipmap.ic_launcher)
                        binding.loadingItem.visibility = View.GONE

                    }

                    override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                        binding.loadingItem.visibility = View.GONE
                        binding.imgThumb.visibility = View.VISIBLE
                        binding.imgThumb.setImageBitmap(bitmap)

                        bitmap?.let { viewModel.saveImage(ThumbnailData(it, reddit.data.created_utc.toString(), reddit.data.title)) }

                    }
                })

        RxView.clicks(binding.imgThumb).subscribe {
            if (reddit.data.thumbnail != ThumbnailEnum.DEFAULT.desc && reddit.data.thumbnail != ThumbnailEnum.SELF.desc)
                reddit.data.url?.let { viewModel.clickItem(it) }
        }

    }
}