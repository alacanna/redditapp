package com.android.grabovoi.feature.home.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.amandalacanna.redditapp.dagger.AppInjector
import com.amandalacanna.redditapp.data.Children
import com.amandalacanna.redditapp.databinding.RedditItemAdapterBinding
import com.amandalacanna.redditapp.features.toplist.viewmodel.TopListViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.ArrayList
import javax.inject.Inject


class RedditAdapter @Inject constructor(private val viewModel: TopListViewModel) : RecyclerView.Adapter<RedditViewHolder>() {
    private var redditList: MutableList<Children> = ArrayList()

    init {
        AppInjector.dagger.inject(this)
    }

    fun setObservableDiseaseList(observableDiseaseList: Observable<List<Children>>) {
        observableDiseaseList
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ redditList ->
                    this@RedditAdapter.redditList.addAll(redditList)
                    this@RedditAdapter.notifyItemInserted(itemCount - redditList.size)

                }, { _ ->
                    this@RedditAdapter.notifyDataSetChanged()
                })
    }


    override fun getItemCount(): Int {
        return redditList.size
    }

    override fun onBindViewHolder(holder: RedditViewHolder?, position: Int) {
        holder?.setReddit(redditList[position])
        holder?.setThumbnail(redditList[position])
        holder?.setDate(redditList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RedditViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val diseaseDataBind = RedditItemAdapterBinding.inflate(layoutInflater, parent, false)
        return RedditViewHolder(diseaseDataBind, viewModel)
    }

    fun getRefreshedList(): MutableList<Children> {
        return redditList
    }

}
