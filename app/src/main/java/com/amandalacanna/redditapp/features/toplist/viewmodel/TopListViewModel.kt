package com.amandalacanna.redditapp.features.toplist.viewmodel

import android.util.Log
import com.amandalacanna.redditapp.api.manager.RedditManager
import com.amandalacanna.redditapp.data.Children
import com.amandalacanna.redditapp.data.RedditData
import com.amandalacanna.redditapp.data.ThumbnailData
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.io.Serializable
import javax.inject.Inject
import javax.inject.Singleton

interface TopListViewModelInputs {
    fun getTopList (after: String?, count: Int?)
    fun clickItem (urlSelected: String)
    fun saveImage (thumbnailData: ThumbnailData)
}

interface TopListViewModelOutputs {
    val isLoading: Observable<Boolean>
    val listReddit: Observable<List<Children>>
    val onItemClicked: Observable<String>
    val setImage: Observable<ThumbnailData>
    val hasError: Observable<String>
    val redditData: Observable<RedditData>

}

interface TopListViewModelType {
    val inputs: TopListViewModelInputs
    val outputs: TopListViewModelOutputs
}

@Singleton
class TopListViewModel @Inject constructor() : TopListViewModelType, TopListViewModelInputs, TopListViewModelOutputs, Serializable {

    @Inject
    lateinit var redditManager: RedditManager

    // Initialize inputs and outputs
    override val inputs: TopListViewModelInputs
        get() = this

    override val outputs: TopListViewModelOutputs
        get() = this


    // Outputs implementations
    private val isLoadingSubject = PublishSubject.create<Boolean>()
    override val isLoading: Observable<Boolean>
        get() = isLoadingSubject

    private val listRedditSubject = PublishSubject.create<List<Children>>()
    override val listReddit: Observable<List<Children>>
        get() = listRedditSubject

    private val hasErrorSubject = PublishSubject.create<String>()
    override val hasError: Observable<String>
        get() = hasErrorSubject

    private val setImageSubject = PublishSubject.create<ThumbnailData>()
    override val setImage: Observable<ThumbnailData>
        get() = setImageSubject

    private val onItemClickedSubject = PublishSubject.create<String>()
    override val onItemClicked: Observable<String>
        get() = onItemClickedSubject

    private val redditDataSubject = PublishSubject.create<RedditData>()
    override val redditData: Observable<RedditData>
        get() = redditDataSubject


    override fun clickItem(urlSelected: String) {
        onItemClickedSubject.onNext(urlSelected)
    }

    override fun saveImage(thumbnailData: ThumbnailData) {
        setImageSubject.onNext(thumbnailData)
    }

    override fun getTopList(after: String?, count: Int?) {
        isLoadingSubject.onNext(true)
        redditManager.getTopReddit(after, count)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            isLoadingSubject.onNext(false)
                            redditDataSubject.onNext(it)
                            it.data.children?.let { listRedditSubject.onNext(it) }
                            Log.d("Log", "Success : "+ listReddit.toString())},
                        {throwable ->
                            isLoadingSubject.onNext(false)
                            throwable.message?.let { hasErrorSubject.onNext(it) }
                            Log.d("Log", "Fail : "+ throwable.message)
                        })
    }


}
