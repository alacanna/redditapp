package com.amandalacanna.redditapp.features.toplist

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import android.widget.Toast
import com.amandalacanna.redditapp.R
import com.amandalacanna.redditapp.features.toplist.viewmodel.TopListViewModel
import com.android.grabovoi.feature.home.adapter.RedditAdapter
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_main.*
import android.support.v4.content.ContextCompat
import com.amandalacanna.redditapp.data.RedditData
import io.reactivex.Observable
import javax.inject.Inject
import android.support.v7.widget.RecyclerView
import android.os.Parcelable
import io.reactivex.schedulers.Schedulers

class MainActivity : AppCompatActivity() {
    private val PERMISSION_WRITE_EXTERNAL_STORAGE = 5001
    private val REDDIT_STATE = "reddit_state"
    private val LIST_STATE = "list_state"

    @Inject
    lateinit var viewModel: TopListViewModel

    @Inject
    lateinit var  disposables: CompositeDisposable

    @Inject
    lateinit var redditAdapter : RedditAdapter

    private var redditInstanceSaved: RedditData? = null
    lateinit var state: Parcelable

    private  val checkPermission by lazy { ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (checkPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), PERMISSION_WRITE_EXTERNAL_STORAGE)
        }else{
            bindOutputs()
            viewModel.inputs.getTopList(null, null)
        }

        initListView()
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        savedInstanceState?.let {
            redditInstanceSaved = it.getSerializable(REDDIT_STATE) as RedditData?
            redditAdapter.setObservableDiseaseList(Observable.just(redditInstanceSaved?.data?.children))

            state = it.getParcelable(LIST_STATE)
            list_reddit.layoutManager.onRestoreInstanceState(state)
        }

    }

    public override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(LIST_STATE, list_reddit.layoutManager.onSaveInstanceState())
        redditInstanceSaved?.data?.children = redditAdapter.getRefreshedList()
        outState.putSerializable(REDDIT_STATE, redditInstanceSaved)
    }

    private fun bindOutputs() = with(viewModel.outputs) {
        val isLoadingDisposable = isLoading.subscribe({ isLoading ->
            if (isLoading)
                progress.visibility = View.VISIBLE
            else
                progress.visibility = View.GONE
        },{throwable ->
            Log.d("Log", "Fail : "+ throwable.message)
        })

        val hasErrorDisposable = hasError.subscribe({
            messsage ->
                Toast.makeText(baseContext, messsage, Toast.LENGTH_LONG).show()
        })

        val onItemClickedDisposable = onItemClicked.subscribe({
            url ->
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
        })

        val setImageDisposable = setImage
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe({ (bitmap, name, description) ->
            MediaStore.Images.Media.insertImage(contentResolver, bitmap, name, description)
        })

        val dataRedditDisposable = redditData.subscribe({
            redditData ->
            redditInstanceSaved = redditData
        })

        disposables.addAll(isLoadingDisposable,
                hasErrorDisposable,
                onItemClickedDisposable,
                setImageDisposable,
                dataRedditDisposable)
    }

    private fun initListView() {
        list_reddit.layoutManager = LinearLayoutManager(baseContext)
        redditAdapter.setObservableDiseaseList(viewModel.outputs.listReddit)

        list_reddit.adapter = redditAdapter

        list_reddit.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
            }
            override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
                val layoutManager: LinearLayoutManager = list_reddit.layoutManager as LinearLayoutManager
                val totalItemCount = layoutManager.itemCount
                val lastVisibleItem = layoutManager.findLastVisibleItemPosition()

                if (totalItemCount > 1) {
                    if (lastVisibleItem == totalItemCount - 1 && totalItemCount < 50) {
                        viewModel.inputs.getTopList(redditInstanceSaved?.data?.after, totalItemCount)
                    }
                }
            }
        })


    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_WRITE_EXTERNAL_STORAGE -> {
                bindOutputs()
                viewModel.inputs.getTopList(null,null)
                return
            }
        }
    }
}
